﻿using DataModels1;
using RepositoryPattern1;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MiniHotelManagement
{
    public partial class MainWindow : Window
    {
        private readonly CustomerRepository _customerRepository;
        private ObservableCollection<Customer> _customers;

        public MainWindow()
        {
            _customerRepository = new CustomerRepository();
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            _customers = new ObservableCollection<Customer>(_customerRepository.GetAllCustomers());
            dgData.ItemsSource = _customers;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Customer newCustomer = CreateCustomerFromInput();
                _customerRepository.AddCustomer(newCustomer);
                _customers.Add(newCustomer);
                ClearInputFields();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error creating customer: {ex.Message}");
            }
        }

        private Customer CreateCustomerFromInput()
        {
            return new Customer
            {
                CustomerID = _customers.Any() ? _customers.Max(c => c.CustomerID) + 1 : 1,
                CustomerFullName = txtFullName.Text,
                Telephone = txtPhone.Text,
                EmailAddress = txtEmail.Text,
                CustomerBirthday = txtDob.Text,
                CustomerStatus = 0,
                Password = txtPassword.Text
            };
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (int.TryParse(txtCustomerID.Text, out int customerId))
                {
                    Customer customerToUpdate = _customers.FirstOrDefault(c => c.CustomerID == customerId);
                    if (customerToUpdate != null)
                    {
                        UpdateCustomerFromInput(customerToUpdate);
                        _customerRepository.UpdateCustomer(customerToUpdate);
                        dgData.Items.Refresh();
                        ClearInputFields();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error updating customer: {ex.Message}");
            }
        }

        private void UpdateCustomerFromInput(Customer customer)
        {
            customer.CustomerFullName = txtFullName.Text;
            customer.Telephone = txtPhone.Text;
            customer.EmailAddress = txtEmail.Text;
            customer.CustomerBirthday = txtDob.Text;
            customer.CustomerStatus = 0;
            customer.Password = txtPassword.Text;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (int.TryParse(txtCustomerID.Text, out int customerId))
                {
                    Customer customerToDelete = _customers.FirstOrDefault(c => c.CustomerID == customerId);
                    if (customerToDelete != null)
                    {
                        _customers.Remove(customerToDelete);
                        _customerRepository.DeleteCustomer(customerId);
                        ClearInputFields();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error deleting customer: {ex.Message}");
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Bạn có muốn thoát ra không?", "Xác nhận thoát", MessageBoxButton.OKCancel, MessageBoxImage.Question);

            // Kiểm tra lựa chọn của người dùng
            if (result == MessageBoxResult.OK)
            {
                // Nếu người dùng chọn OK, đóng cửa sổ
                Login_Window login = new Login_Window();
                login.Show();
                this.Close();
            }
        }

        private void dgData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgData.SelectedItem is Customer selectedCustomer)
            {
                PopulateInputFields(selectedCustomer);
            }
        }

        private void PopulateInputFields(Customer customer)
        {
            txtCustomerID.Text = customer.CustomerID.ToString();
            txtFullName.Text = customer.CustomerFullName;
            txtPhone.Text = customer.Telephone;
            txtEmail.Text = customer.EmailAddress;
            txtDob.Text = customer.CustomerBirthday;
            txtPassword.Text = customer.Password;
        }

        private void ClearInputFields()
        {
            txtCustomerID.Clear();
            txtFullName.Clear();
            txtPhone.Clear();
            txtEmail.Clear();
            txtDob.Clear();
            txtPassword.Clear();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            RoomManagerWindow roomManager = new RoomManagerWindow();
            roomManager.Show();
        }
    }
}
