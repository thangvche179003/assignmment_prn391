﻿using DataModels1;
using RepositoryPattern1;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MiniHotelManagement
{
    /// <summary>
    /// Interaction logic for RoomManagerWindow.xaml
    /// </summary>
    public partial class RoomManagerWindow : Window
    {
        private readonly RoomRepository _roomRepository;
        private readonly RoomTypeRepository _roomTypeRepository;
        private ObservableCollection<RoomInformation> _rooms;
        private ObservableCollection<RoomType> _roomTypes;

        public RoomManagerWindow()
        {
            _roomRepository = new RoomRepository();
            _roomTypeRepository = new RoomTypeRepository();
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDataRooms();
            LoadDataRoomTypes();
        }

        private void LoadDataRooms()
        {
            _rooms = new ObservableCollection<RoomInformation>(_roomRepository.GetAllRooms());
            dgData.ItemsSource = _rooms;
        }

        private void LoadDataRoomTypes()
        {
            _roomTypes = new ObservableCollection<RoomType>(_roomTypeRepository.GetRoomTypes());
            cboCategory.ItemsSource = _roomTypes;
            cboCategory.DisplayMemberPath = "RoomTypeName";
            cboCategory.SelectedValuePath = "RoomTypeID";
        }

        private void dgData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgData.SelectedItem is RoomInformation room)
            {
                PopulateRoomDetails(room);
            }
        }

        private void PopulateRoomDetails(RoomInformation room)
        {
            txtRoomID.Text = room.RoomID.ToString();
            txtRoomNumber.Text = room.RoomNumber;
            txtRoomDescription.Text = room.RoomDescription;
            txtRoomCapacity.Text = room.RoomMaxCapacity.ToString();
            txtRoomPrice.Text = room.RoomPricePerDate.ToString();
            cboCategory.SelectedValue = room.RoomTypeID;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RoomInformation newRoom = new RoomInformation
                {
                    RoomID = GetNewRoomId(),
                    RoomNumber = txtRoomNumber.Text,
                    RoomDescription = txtRoomDescription.Text,
                    RoomMaxCapacity = int.Parse(txtRoomCapacity.Text),
                    RoomPricePerDate = decimal.Parse(txtRoomPrice.Text),
                    RoomStatus = 1,
                    RoomTypeID = (int)cboCategory.SelectedValue
                };

                _roomRepository.AddRoom(newRoom);
                LoadDataRooms();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error creating room: {ex.Message}");
            }
        }

        private int GetNewRoomId()
        {
            var lastRoom = _roomRepository.GetAllRooms().LastOrDefault();
            return (lastRoom?.RoomID ?? 0) + 1;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (int.TryParse(txtRoomID.Text, out int roomId))
                {
                    RoomInformation roomToUpdate = _roomRepository.GetRoomById(roomId);

                    if (roomToUpdate != null)
                    {
                        UpdateRoomDetails(roomToUpdate);
                        _roomRepository.UpdateRoom(roomToUpdate);
                        LoadDataRooms();
                    }
                    else
                    {
                        MessageBox.Show("Room not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error updating room: {ex.Message}");
            }
        }

        private void UpdateRoomDetails(RoomInformation room)
        {
            room.RoomNumber = txtRoomNumber.Text;
            room.RoomDescription = txtRoomDescription.Text;
            room.RoomMaxCapacity = int.Parse(txtRoomCapacity.Text);
            room.RoomPricePerDate = decimal.Parse(txtRoomPrice.Text);
            room.RoomTypeID = (int)cboCategory.SelectedValue;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (int.TryParse(txtRoomID.Text, out int roomId))
                {
                    _roomRepository.DeleteRoom(roomId);
                    LoadDataRooms();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error deleting room: {ex.Message}");
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
